/*
 * Copyright 2008 Web Cohesion
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.oauth.serviceprovider.internal;

import net.oauth.OAuth;
import net.oauth.OAuthProblemException;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * This test class is mostly based on:-
 * https://fisheye.springsource.org/browse/spring-security-oauth/spring-security-oauth/src/test/java/org/springframework/security/oauth/provider/nonce/TestInMemoryNonceServices.java?r=d8ac98c84159e2ee0d1a68960a59f20c8600a4a5&r=d8ac98c84159e2ee0d1a68960a59f20c8600a4a5
 */
public class InMemoryNonceServicesTest {
    private long now;
    private InMemoryNonceService nonceServices;

    @Before
    public void setUp() throws Exception {
        // seconds since epoch, reset for every test
        now = System.currentTimeMillis() / 1000;
        nonceServices = new InMemoryNonceService(10);
        InMemoryNonceService.NONCES.clear();
    }

    @Test
    public void shouldAcceptSameNonceWithDifferentTimestamp() throws OAuthProblemException {
        String nonce = nonce();
        nonceServices.validateNonce("foo", now, nonce);
        nonceServices.validateNonce("foo", now + 5, nonce);
        assertEquals(2, InMemoryNonceService.NONCES.size());
    }

    @Test
    public void shouldRejectAlreadyUsedNonceWithSameTimestamp() throws OAuthProblemException {
        String nonce = nonce();
        nonceServices.validateNonce("foo", now, nonce);
        try {
            nonceServices.validateNonce("foo", now, nonce);
            fail("expected exception");
        } catch (OAuthProblemException ope) {
            assertEquals(OAuth.Problems.NONCE_USED, ope.getProblem());
        }
    }

    @Test
    public void shouldAcceptSameNonceFromDifferentConsumers() throws OAuthProblemException {
        String nonce = nonce();
        nonceServices.validateNonce("foo", now, nonce);
        nonceServices.validateNonce("bar", now, nonce);
        assertEquals(2, InMemoryNonceService.NONCES.size());
    }

    @Test
    public void shouldRemoveOldNonces() throws OAuthProblemException {
        // order should not matter
        InMemoryNonceService.NONCES.add(new InMemoryNonceService.NonceEntry("foo", now - 2, nonce()));
        InMemoryNonceService.NONCES.add(new InMemoryNonceService.NonceEntry("foo", now - 11, nonce()));
        InMemoryNonceService.NONCES.add(new InMemoryNonceService.NonceEntry("foo", now + 8, nonce()));
        InMemoryNonceService.NONCES.add(new InMemoryNonceService.NonceEntry("foo", now - 15, nonce()));
        assertEquals(4, InMemoryNonceService.NONCES.size());
        nonceServices.validateNonce("foo", now, nonce());
        assertEquals("should have removed two from the original four nonces and added one", 3, InMemoryNonceService.NONCES.size());
    }

    private String nonce() {
        return UUID.randomUUID().toString();
    }
}

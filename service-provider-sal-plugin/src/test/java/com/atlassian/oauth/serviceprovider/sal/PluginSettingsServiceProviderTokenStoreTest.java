package com.atlassian.oauth.serviceprovider.sal;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.event.AccessTokenRemovedEvent;
import com.atlassian.oauth.event.RequestTokenRemovedEvent;
import com.atlassian.oauth.event.TokenRemovedEvent;
import com.atlassian.oauth.serviceprovider.Clock;
import com.atlassian.oauth.serviceprovider.InvalidTokenException;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.ServiceProviderTokenBuilder;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.sal.PluginSettingsServiceProviderTokenStore.ServiceProviderTokenProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.google.common.base.Function;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Session.newSession;
import static com.atlassian.oauth.serviceprovider.sal.PluginSettingsServiceProviderTokenStore.Settings.CONSUMER_TOKENS;
import static com.atlassian.oauth.serviceprovider.sal.PluginSettingsServiceProviderTokenStore.Settings.KEY_LIST_PROPERTY;
import static com.atlassian.oauth.serviceprovider.sal.PluginSettingsServiceProviderTokenStore.Settings.TOKEN_KEYS;
import static com.atlassian.oauth.serviceprovider.sal.PluginSettingsServiceProviderTokenStore.Settings.TOKEN_PREFIX;
import static com.atlassian.oauth.serviceprovider.sal.PluginSettingsServiceProviderTokenStore.Settings.USER_ACCESS_TOKENS;
import static com.atlassian.oauth.shared.sal.Functions.toEncodedKeys;
import static com.atlassian.oauth.testing.Matchers.equalTo;
import static com.atlassian.oauth.testing.Matchers.hasEntries;
import static com.atlassian.oauth.testing.Matchers.mapWithKeys;
import static com.atlassian.oauth.testing.Matchers.withStringLength;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER_WITH_LONG_KEY;
import static com.atlassian.oauth.testing.TestData.USER;
import static com.atlassian.oauth.testing.TestData.USER_WITH_LONG_USERNAME;
import static com.google.common.collect.Iterables.toArray;
import static com.google.common.collect.Iterables.transform;
import static java.util.Arrays.asList;
import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.math.RandomUtils.nextBoolean;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PluginSettingsServiceProviderTokenStoreTest {
    private static final URI CALLBACK = URI.create("http://consumer/callback");
    private static final String VERIFIER = "9876";
    private static final long TIME = 0L;
    private static final long TIME_AFTER = 1L;
    private static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).callback(CALLBACK).version(ServiceProviderToken.Version.V_1_0_A).build();
    private static final Properties UNAUTHORIZED_REQUEST_TOKEN_AS_PROPERTIES = new ServiceProviderTokenProperties(UNAUTHORIZED_REQUEST_TOKEN).asProperties();
    private static final ServiceProviderToken AUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).authorizedBy(USER).verifier(VERIFIER).callback(CALLBACK).version(ServiceProviderToken.Version.V_1_0_A).build();
    private static final Properties AUTHORIZED_REQUEST_TOKEN_AS_PROPERTIES = new ServiceProviderTokenProperties(AUTHORIZED_REQUEST_TOKEN).asProperties();
    private static final ServiceProviderToken DENIED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).deniedBy(USER).callback(CALLBACK).version(ServiceProviderToken.Version.V_1_0_A).build();
    private static final Properties DENIED_REQUEST_TOKEN_AS_PROPERTIES = new ServiceProviderTokenProperties(DENIED_REQUEST_TOKEN).asProperties();
    private static final ServiceProviderToken ACCESS_TOKEN = ServiceProviderToken.newAccessToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).authorizedBy(USER).session(newSession("abcd").creationTime(TIME).lastRenewalTime(TIME_AFTER).build()).build();
    private static final Properties ACCESS_TOKEN_AS_PROPERTIES = new ServiceProviderTokenProperties(ACCESS_TOKEN).asProperties();
    private static final String TOKEN_KEY_SEPARATOR = "/";
    private static final ServiceProviderToken ACCESS_TOKEN_WITH_LONG_CONSUMER_KEY = ServiceProviderToken.newAccessToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER_WITH_LONG_KEY).authorizedBy(USER).session(newSession("abcd").creationTime(TIME).lastRenewalTime(TIME_AFTER).build()).build();
    private static final ServiceProviderToken ACCESS_TOKEN_WITH_LONG_USERNAME = ServiceProviderToken.newAccessToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).authorizedBy(USER_WITH_LONG_USERNAME).session(newSession("abcd").creationTime(TIME).lastRenewalTime(TIME_AFTER).build()).build();

    @Mock
    PluginSettingsFactory pluginSettingsFactory;
    @Mock
    ServiceProviderConsumerStore consumerStore;
    @Mock
    UserManager userManager;
    @Mock
    Clock clock;
    @Mock
    EventPublisher eventPublisher;

    Map<String, Object> settings;
    ServiceProviderTokenStore store;

    @Before
    public void setUp() {
        settings = new HashMap<String, Object>();
        when(userManager.resolve(USER.getName())).thenReturn(USER);
        when(consumerStore.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);
        when(pluginSettingsFactory.createGlobalSettings()).thenReturn(new MapBackedPluginSettings(settings));
        store = new PluginSettingsServiceProviderTokenStore(pluginSettingsFactory, consumerStore, userManager, clock, eventPublisher);
    }

    @Test
    public void assertThatRequestTokensAreStoredAsProperties() {
        store.put(UNAUTHORIZED_REQUEST_TOKEN);

        assertThat(
                (Properties) settings.get(tokenKey("1234")),
                is(equalTo(UNAUTHORIZED_REQUEST_TOKEN_AS_PROPERTIES))
        );
    }

    private static String tokenKey(String token) {
        return ServiceProviderTokenStore.class.getName() + "." + TOKEN_PREFIX + "." + token;
    }

    private static String tokenKeys() {
        return ServiceProviderTokenStore.class.getName() + "." + TOKEN_KEYS;
    }

    @Test
    public void assertThatAccessTokensAreStoredAsProperties() {
        store.put(ACCESS_TOKEN);

        assertThat(
                (Properties) settings.get(tokenKey("1234")),
                is(equalTo(ACCESS_TOKEN_AS_PROPERTIES))
        );
    }

    @Test
    public void assertThatPutAddsTokenToConsumerTokens() {
        String consumerTokensKey = consumerTokensKey(UNAUTHORIZED_REQUEST_TOKEN.getConsumer().getKey());
        settings.put(consumerTokensKey, "a/bunch/of/tokens");
        store.put(UNAUTHORIZED_REQUEST_TOKEN);
        assertThat(getKeyListString(consumerTokensKey), containsString(UNAUTHORIZED_REQUEST_TOKEN.getToken()));
    }

    private String getKeyListString(String key) {
        Object value = settings.get(key);
        return (value instanceof Properties) ? (String) ((Properties) settings.get(key)).get(KEY_LIST_PROPERTY) : (String) value;
    }

    private String consumerTokensKey(String consumerKey) {
        return ServiceProviderTokenStore.class.getName() + "." + CONSUMER_TOKENS + "." + consumerKey;
    }

    @Test
    public void assertThatRemovingTokenRemovesItFromAllTokenSet() {
        settings.put(tokenKeys(), "a/bunch/of/tokens/" + UNAUTHORIZED_REQUEST_TOKEN.getToken());
        settings.put(tokenKey("1234"), UNAUTHORIZED_REQUEST_TOKEN_AS_PROPERTIES);

        store.removeAndNotify(UNAUTHORIZED_REQUEST_TOKEN.getToken());

        assertThat((String) settings.get(tokenKeys()), not(containsString(UNAUTHORIZED_REQUEST_TOKEN.getToken())));
    }

    @Test
    public void assertThatRemovingTokenRemovesItFromConsumerTokenSet() {
        String consumerTokensKey = consumerTokensKey(RSA_CONSUMER.getKey());
        settings.put(consumerTokensKey, "a/bunch/of/tokens/" + UNAUTHORIZED_REQUEST_TOKEN.getToken());
        settings.put(tokenKey("1234"), UNAUTHORIZED_REQUEST_TOKEN_AS_PROPERTIES);

        store.removeAndNotify(UNAUTHORIZED_REQUEST_TOKEN.getToken());

        assertThat(getKeyListString(consumerTokensKey), not(containsString(UNAUTHORIZED_REQUEST_TOKEN.getToken())));
    }

    @Test
    public void assertThatRemovingAccessTokenPublishesRemovedEvent() {
        settings.put(tokenKey("1234"), ACCESS_TOKEN_AS_PROPERTIES);

        store.removeAndNotify(ACCESS_TOKEN.getToken());

        verify(eventPublisher).publish(isA(AccessTokenRemovedEvent.class));
    }

    @Test
    public void assertThatRemovingRequestTokenPublishesRemovedEvent() {
        settings.put(tokenKey("1234"), UNAUTHORIZED_REQUEST_TOKEN_AS_PROPERTIES);

        store.removeAndNotify(UNAUTHORIZED_REQUEST_TOKEN.getToken());

        verify(eventPublisher).publish(isA(RequestTokenRemovedEvent.class));
    }

    @Test
    public void assertThatGetReturnsProperlyConstructedAccessToken() {
        settings.put(
                tokenKey("1234"),
                ACCESS_TOKEN_AS_PROPERTIES
        );
        assertThat(store.get("1234"), is(equalTo(ACCESS_TOKEN)));
    }

    @Test(expected = InvalidTokenException.class)
    public void assertThatDeletedUserProperlyReportsInvalidToken() {
        settings.put(
                tokenKey("1234"),
                ACCESS_TOKEN_AS_PROPERTIES
        );
        when(userManager.resolve(USER.getName())).thenReturn(null);
        store.get("1234");
    }

    @Test
    public void assertThatInvalidTokenIsRemovedWhenDeletedUserTokenIsChecked() {
        settings.put(
                tokenKey("1234"),
                ACCESS_TOKEN_AS_PROPERTIES
        );
        when(userManager.resolve(USER.getName())).thenReturn(null);

        try {
            store.get("1234");
        } catch (InvalidTokenException e) {
            //expected
        }

        assertThat(store.get("1234"), is(Matchers.nullValue()));
    }

    @Test
    public void assertThatGetReturnsProperlyConstructedAuthorizedRequestToken() {
        settings.put(
                tokenKey("1234"),
                AUTHORIZED_REQUEST_TOKEN_AS_PROPERTIES
        );
        assertThat(store.get("1234"), is(equalTo(AUTHORIZED_REQUEST_TOKEN)));
    }

    @Test
    public void assertThatGetReturnsProperlyConstructedDeniedRequestToken() {
        settings.put(
                tokenKey("1234"),
                DENIED_REQUEST_TOKEN_AS_PROPERTIES
        );
        assertThat(store.get("1234"), is(equalTo(DENIED_REQUEST_TOKEN)));
    }

    @Test
    public void assertThatRemoveExpiredTokensOnlyRemovesTokensThatHaveExpired() {
        when(clock.timeInMilliseconds()).thenReturn(TIME);
        Map<String, Object> expiredTokens = putExpiredTokens(clock);
        Map<String, Object> nonExpiredTokens = putNonExpiredTokens(clock);

        store.removeExpiredTokensAndNotify();

        assertThat(settings, allOf(hasEntries(nonExpiredTokens), not(hasEntries(expiredTokens))));
    }

    @Test
    public void assertThatRemoveExpiredTokensDoesNotChokeOnInvalidTokenReferences() {
        settings.put(tokenKeys(), "invalid/token/refs");
        putExpiredTokens(clock);

        store.removeExpiredTokensAndNotify();

        assertThat((String) settings.get(tokenKeys()), is(equalTo("")));
    }

    @Test
    public void assertThatRemoveExpiredTokensDoesNotChokeOnDeletedUsers() {
        final String accessTokenKey = "acc1234";
        final String requestTokenKey = "req1234";

        putExpiredTokens(clock);

        ServiceProviderTokenBuilder accessToken = ServiceProviderToken.newAccessToken(accessTokenKey).tokenSecret("5678").consumer(RSA_CONSUMER).authorizedBy(USER);

        addAsExpiredTokenForDeletedUser(settings, accessToken, ServiceProviderToken.DEFAULT_ACCESS_TOKEN_TTL, tokenKeys());

        ServiceProviderTokenBuilder authorizedRequestToken = ServiceProviderToken.newRequestToken(requestTokenKey).tokenSecret("5678").consumer(RSA_CONSUMER)
                .authorizedBy(USER).verifier(VERIFIER).callback(CALLBACK).version(Version.V_1_0_A);

        addAsExpiredTokenForDeletedUser(settings, authorizedRequestToken, ServiceProviderToken.DEFAULT_REQUEST_TOKEN_TTL, tokenKeys());

        assertThat((String) settings.get(tokenKeys()), containsString(accessTokenKey));
        assertThat((String) settings.get(tokenKeys()), containsString(requestTokenKey));

        store.removeExpiredTokensAndNotify();

        assertThat((String) settings.get(tokenKeys()), is(equalTo("")));
    }

    @Test
    public void assertThatRemoveExpiredTokensPublishesTokenRemovedEvent() {
        when(clock.timeInMilliseconds()).thenReturn(TIME);
        Map<String, Object> expiredTokensAndTokenKeys = putExpiredTokens(clock);
        int numberOfExpiredTokens = expiredTokensAndTokenKeys.size() - 1;

        assertThat("Cannot perform test with no expired tokens", numberOfExpiredTokens, greaterThan(0));

        store.removeExpiredTokensAndNotify();

        verify(eventPublisher, times(numberOfExpiredTokens)).publish(isA(TokenRemovedEvent.class));
    }

    @Test
    public void assertThatRemoveByConsumerDoesNotChokeOnDeletedUsers() {
        final String requestTokenKey = "req1234";

        Consumer consumer = RSA_CONSUMER;
        ServiceProviderTokenBuilder authorizedRequestToken = ServiceProviderToken.newRequestToken(requestTokenKey).tokenSecret("5678").consumer(consumer)
                .authorizedBy(USER).verifier(VERIFIER).callback(CALLBACK).version(Version.V_1_0_A);

        String tokenListKey = consumerTokensKey(consumer.getKey());
        addAsExpiredTokenForDeletedUser(settings, authorizedRequestToken, ServiceProviderToken.DEFAULT_REQUEST_TOKEN_TTL, tokenListKey);

        assertThat(getKeyListString(tokenListKey), containsString(requestTokenKey));

        store.removeByConsumer(consumer.getKey());

        assertThat(getKeyListString(tokenListKey), is(equalTo("")));
    }

    @Test
    public void assertThatRemoveByConsumerPublishesTokenRemovedEvent() {
        String consumerTokensKey = consumerTokensKey(RSA_CONSUMER.getKey());
        settings.put(consumerTokensKey, "a/bunch/of/tokens/" + UNAUTHORIZED_REQUEST_TOKEN.getToken());
        settings.put(tokenKey("1234"), UNAUTHORIZED_REQUEST_TOKEN_AS_PROPERTIES);

        store.removeByConsumer(RSA_CONSUMER.getKey());

        verify(eventPublisher).publish(isA(RequestTokenRemovedEvent.class));
    }

    private void addAsExpiredTokenForDeletedUser(Map<String, Object> settings, ServiceProviderTokenBuilder accessToken, long ttl, String tokenListKey) {
        ServiceProviderToken expiredToken = setExpiredTime(clock, accessToken, ttl).build();

        Properties expiredTokenForDeletedUser = new ServiceProviderTokenProperties(expiredToken).asProperties();
        expiredTokenForDeletedUser.put(ServiceProviderTokenProperties.USER_NAME, "XXX");

        settings.put(
                tokenKey(expiredToken.getToken()),
                expiredTokenForDeletedUser
        );
        addTokenKey(settings, expiredToken.getToken(), tokenListKey);
    }

    @Test
    public void assertThatGetUserAccessTokensReturnsTokensForThatUser() {
        ServiceProviderToken extraAccessToken = ServiceProviderToken.newAccessToken("4321").tokenSecret("5678").consumer(RSA_CONSUMER).authorizedBy(USER).build();
        Properties extraAccessTokenProperties = new ServiceProviderTokenProperties(extraAccessToken).asProperties();

        settings.put(
                userAccessTokensKey(USER.getName()),
                ACCESS_TOKEN.getToken() + TOKEN_KEY_SEPARATOR + extraAccessToken.getToken()
        );
        settings.put(
                tokenKey(ACCESS_TOKEN.getToken()),
                ACCESS_TOKEN_AS_PROPERTIES
        );
        settings.put(
                tokenKey(extraAccessToken.getToken()),
                extraAccessTokenProperties
        );

        assertThat(store.getAccessTokensForUser(USER.getName()), contains(ACCESS_TOKEN, extraAccessToken));
    }

    @Test
    public void assertThatInvalidTokenReferenceIsRemovedWhenTokensAreModified() {
        settings.put(tokenKeys(), "invalid/token/refs");
        store.put(UNAUTHORIZED_REQUEST_TOKEN);
        assertThat((String) settings.get(tokenKeys()), is(equalTo(UNAUTHORIZED_REQUEST_TOKEN.getToken())));
    }

    @Test
    public void assertThatInvalidConsumerTokenReferenceIsRemovedWhenTokensAreModified() {
        settings.put(consumerTokensKey(RSA_CONSUMER.getKey()), "invalid/token/refs");
        store.put(UNAUTHORIZED_REQUEST_TOKEN);
        assertThat(getKeyListString(consumerTokensKey(RSA_CONSUMER.getKey())), is(equalTo(UNAUTHORIZED_REQUEST_TOKEN.getToken())));
    }

    @Test
    public void assertThatInvalidUserAccessTokenReferenceIsRemovedWhenTokensAreModified() {
        settings.put(userAccessTokensKey(USER.getName()), "invalid/token/refs");
        store.put(ACCESS_TOKEN);
        assertThat(getKeyListString(userAccessTokensKey(USER.getName())), is(equalTo(UNAUTHORIZED_REQUEST_TOKEN.getToken())));
    }

    @Test
    public void assertThatMissingRequestTokenVersionWithCallbackInStoreDefaultsToVersion1a() {
        Properties requestTokenProperties = new ServiceProviderTokenProperties(UNAUTHORIZED_REQUEST_TOKEN).asProperties();
        requestTokenProperties.remove(ServiceProviderTokenProperties.VERSION);
        settings.put(tokenKey("1234"), requestTokenProperties);

        assertThat(store.get("1234").getVersion(), is(equalTo(Version.V_1_0_A)));
    }

    @Test
    public void assertThatMissingRequestTokenVersionWithoutCallbackInStoreDefaultsToVersion1() {
        Properties requestTokenProperties = new ServiceProviderTokenProperties(UNAUTHORIZED_REQUEST_TOKEN).asProperties();
        requestTokenProperties.remove(ServiceProviderTokenProperties.VERSION);
        requestTokenProperties.remove(ServiceProviderTokenProperties.CALLBACK);
        settings.put(tokenKey("1234"), requestTokenProperties);

        assertThat(store.get("1234").getVersion(), is(equalTo(Version.V_1_0)));
    }

    @Test
    public void assertThatConsumerTokenKeysAreLessThanOneHundredCharacters() {
        store.put(ACCESS_TOKEN_WITH_LONG_CONSUMER_KEY);

        assertThat(settings, mapWithKeys(withStringLength(lessThanOrEqualTo(100))));
    }

    @Test
    public void assertThatUserAccessTokenKeysAreLessThanOneHundredCharacters() {
        store.put(ACCESS_TOKEN_WITH_LONG_USERNAME);

        assertThat(settings, mapWithKeys(withStringLength(lessThanOrEqualTo(100))));
    }

    private String userAccessTokensKey(String name) {
        return ServiceProviderTokenStore.class.getName() + "." + USER_ACCESS_TOKENS + "." + name;
    }

    private Matcher<Iterable<?>> contains(ServiceProviderToken... tokens) {
        Iterable<Matcher> matchers = transform(asList(tokens), new Function<ServiceProviderToken, Matcher>() {
            public Matcher<ServiceProviderToken> apply(ServiceProviderToken token) {
                return (Matcher<ServiceProviderToken>) equalTo(token);
            }
        });

        return Matchers.contains(toArray(matchers, Matcher.class));
    }

    private Map<String, Object> putNonExpiredTokens(Clock clock) {
        return putTokens(clock, false, settings);
    }

    private Map<String, Object> putExpiredTokens(Clock clock) {
        return putTokens(clock, true, settings);
    }

    private static Map<String, Object> putTokens(Clock clock, boolean expired, Map<String, Object> settings) {
        Map<String, Object> newTokens = new HashMap<String, Object>();
        for (int i = 0; i < 5; i++) // why 5? seems like a good, round number.  it could be 20, it could be 10, it doesn't really matter.
        {
            ServiceProviderTokenBuilder tokenBuilder;
            long ttl;
            if (nextBoolean()) {
                tokenBuilder = ServiceProviderToken.newRequestToken((expired ? "expired-" : "") + "request-token-" + randomAlphanumeric(10)).version(Version.V_1_0_A);
                ttl = ServiceProviderToken.DEFAULT_REQUEST_TOKEN_TTL;
            } else {
                tokenBuilder = ServiceProviderToken.newAccessToken((expired ? "expired-" : "") + "access-token-" + randomAlphanumeric(10)).authorizedBy(USER);
                ttl = ServiceProviderToken.DEFAULT_ACCESS_TOKEN_TTL;
            }

            if (expired) {
                setExpiredTime(clock, tokenBuilder, ttl);
            }
            ServiceProviderToken token = tokenBuilder.consumer(RSA_CONSUMER).tokenSecret(randomAlphanumeric(10)).build();

            putToken(newTokens, token);
        }

        settings.putAll(newTokens);
        return newTokens;
    }

    private static ServiceProviderTokenBuilder setExpiredTime(Clock clock, ServiceProviderTokenBuilder tokenBuilder, long defaultTtl) {
        tokenBuilder.creationTime(clock.timeInMilliseconds() - 2 * defaultTtl);
        return tokenBuilder;
    }

    private static void putToken(Map<String, Object> settings, ServiceProviderToken token) {
        settings.put(
                tokenKey(token.getToken()),
                new ServiceProviderTokenProperties(token).asProperties()
        );
        addTokenKey(settings, token.getToken(), tokenKeys());
    }

    private static void addTokenKey(Map<String, Object> settings, String token, String tokenListKey) {
        String originalTokenKeys = (String) settings.get(tokenKeys());
        String newEncodedKey = toEncodedKeys().apply(token);
        String newTokenKeys;
        if (!isBlank(originalTokenKeys)) {
            newTokenKeys = originalTokenKeys + TOKEN_KEY_SEPARATOR + newEncodedKey;
        } else {
            newTokenKeys = newEncodedKey;
        }
        settings.put(tokenListKey, newTokenKeys);
    }
}

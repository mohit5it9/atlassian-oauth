package com.atlassian.webdriver.oauth.page;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

/**
 * Represents the Authorize page.
 *
 * @since v1.9.0
 */
public class AuthorizePage extends AbstractOAuthPage {
    // the active oauth token.
    private final String oauthToken;

    @ElementBy(xpath = "//div[@id='container']/p/strong")
    private PageElement consumerName;

    @ElementBy(id = "approve")
    private PageElement approve;

    @ElementBy(id = "deny")
    private PageElement deny;

    public AuthorizePage(final String oauthToken) {
        this.oauthToken = oauthToken;
    }

    @Override
    public String getUrl() {
        return AUTHORIZE_URL + "?oauth_token=" + this.oauthToken;
    }

    public String getConsumerName() {
        Poller.waitUntilTrue(consumerName.timed().isPresent());
        return consumerName.getText();
    }

    public boolean isApprovePresentAndEnabled() {
        Poller.waitUntilTrue(approve.timed().isPresent());
        return approve.isEnabled();
    }

    public AuthorizePage clickApprove() {
        Poller.waitUntilTrue(approve.timed().isPresent());
        approve.click();
        return this;
    }

    public boolean isDenyPresentAndEnabled() {
        Poller.waitUntilTrue(deny.timed().isPresent());
        return deny.isEnabled();
    }

    public AuthorizePage clickDeny() {
        Poller.waitUntilTrue(deny.timed().isPresent());
        deny.click();
        return this;
    }
}

package com.atlassian.oauth.shared.sal;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * Utility methods for converting between different types of objects and {@link Properties}.
 */
public final class PropertiesHelper {
    /**
     * Returns a {@code Properties} object containing the values of the {@code properties} {@code Map}.
     *
     * @param properties key/value pairs to be inserted into the returned {@code Properties} object
     * @return {@code Properties} object containing the values of the {@code properties} {@code Map}
     */
    public static Properties asProperties(Map<String, String> properties) {
        Properties props = new Properties();
        props.putAll(properties);
        return props;
    }

    /**
     * Returns the properties as a serialized string.  The format is the standard {@code key=value} format, not the
     * XML format.
     *
     * @param props {@code Properties} object to serialize to a string
     * @return the properties as a serialized string
     */
    public static String toString(Properties props) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            // we consume comments because props.store always puts a date/time stamp in the output, which causes
            // problems during testing
            props.store(baos, null);
        } catch (IOException e) {
            throw new AssertionError("ByteArrayOutputStream should never throw an IOException");
        }
        try {
            return PROPERTIES_COMMENT_PATTERN.matcher(baos.toString("ISO-8859-1")).replaceAll("");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("It appears your JVM doesn't support ISO-8859-1 which is a required charset");
        }
    }

    private static final Pattern PROPERTIES_COMMENT_PATTERN = Pattern.compile("^#.*$", Pattern.MULTILINE);

    /**
     * Returns the {@code Properties} object deserialized from the string.  Expects the string to be in the standard
     * {@code key=value} format, not the XML format.
     *
     * @param str serialized string of properties
     * @return {@code Properties} object deserialized from the string
     */
    public static Properties fromString(String str) {
        Properties props = new Properties();
        try {
            props.load(new ByteArrayInputStream(str.getBytes("ISO-8859-1")));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("It appears your JVM doesn't support ISO-8859-1 which is a required charset");
        } catch (IOException e) {
            throw new AssertionError("ByteArrayInputStream can't throw IOException");
        }
        return props;
    }
}

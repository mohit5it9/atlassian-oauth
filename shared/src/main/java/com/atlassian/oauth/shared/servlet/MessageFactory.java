package com.atlassian.oauth.shared.servlet;

import com.atlassian.sal.api.message.I18nResolver;

import java.io.Serializable;

import static com.google.common.base.Preconditions.checkNotNull;

public class MessageFactory {
    private final I18nResolver resolver;

    public MessageFactory(I18nResolver resolver) {
        this.resolver = checkNotNull(resolver, "resolver");
    }

    public Message newMessage(String key, Serializable... params) {
        return new Message(resolver, key, params);
    }
}
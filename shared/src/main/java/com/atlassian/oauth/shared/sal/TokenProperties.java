package com.atlassian.oauth.shared.sal;

import com.atlassian.oauth.Token;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Properties;

/**
 * A wrapper class converting between {@code Token}s and {@code Properties}.  Encapsulates the details of how the
 * token attributes appear in the properties.
 */
public abstract class TokenProperties extends AbstractSettingsProperties {
    static final String TOKEN = "token";
    static final String TOKEN_SECRET = "tokenSecret";
    static final String TYPE = "type";
    static final String CONSUMER_KEY = "consumerKey";
    static final String PROPERTIES = "properties";

    /**
     * Creates a new {@code TokenProperties} instance using the attributes of the {@code Token} as property values.
     */
    public TokenProperties(Token token) {
        putToken(token.getToken());
        putTokenSecret(token.getTokenSecret());
        putTokenType(token.isAccessToken() ? TokenType.ACCESS : TokenType.REQUEST);
        putConsumerKey(token.getConsumer().getKey());
        if (!token.getProperties().isEmpty()) {
            putProperties(token.getProperties());
        }
    }

    /**
     * Creates a new {@code TokenProperties} instance using the values of the {@code Properties} object.
     */
    public TokenProperties(Properties properties) {
        super(properties);
    }

    /**
     * Returns the token value.
     *
     * @return the token value
     */
    public String getToken() {
        return get(TOKEN);
    }

    /**
     * Returns the token secret.
     *
     * @return the token secret
     */
    public String getTokenSecret() {
        return get(TOKEN_SECRET);
    }

    /**
     * Returns {@code true} if the token is an access token, {@code false} otherwise.
     *
     * @return {@code true} if the token is an access token, {@code false} otherwise
     */
    public boolean isAccessToken() {
        return TokenType.ACCESS == getTokenType();
    }

    private TokenType getTokenType() {
        return TokenType.valueOf(get(TYPE));
    }

    /**
     * Returns the key of the consumer this token was created for.
     *
     * @return the key of the consumer this token was created for
     */
    public String getConsumerKey() {
        return get(CONSUMER_KEY);
    }

    /**
     * Returns the properties associated with the token.
     *
     * @return the properties associated with the token
     */
    public Map<String, String> getProperties() {
        String propStr = get(PROPERTIES);
        if (propStr == null) {
            return ImmutableMap.of();
        }
        return Maps.fromProperties(PropertiesHelper.fromString(propStr));
    }

    private void putToken(String token) {
        put(TOKEN, token);
    }

    private void putTokenSecret(String tokenSecret) {
        put(TOKEN_SECRET, tokenSecret);
    }

    private void putTokenType(TokenType type) {
        put(TYPE, type != null ? type.name() : null);
    }

    private void putConsumerKey(String consumerKey) {
        put(CONSUMER_KEY, consumerKey);
    }

    private void putProperties(Map<String, String> properties) {
        put(PROPERTIES, PropertiesHelper.toString(PropertiesHelper.asProperties(properties)));
    }

    private static enum TokenType {
        ACCESS, REQUEST
    }
}
package it.com.atlassian.oauth;

import com.atlassian.webdriver.oauth.page.AuthorizePage;
import com.google.common.collect.ImmutableList;
import net.oauth.OAuth;
import net.oauth.OAuth.Parameter;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.client.OAuthClient;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Map;

import static com.atlassian.oauth.Request.OAUTH_AUTHORIZATION_EXPIRES_IN;
import static com.atlassian.oauth.Request.OAUTH_EXPIRES_IN;
import static com.atlassian.oauth.Request.OAUTH_SESSION_HANDLE;
import static net.oauth.OAuth.OAUTH_CALLBACK;
import static net.oauth.OAuth.OAUTH_CALLBACK_CONFIRMED;
import static net.oauth.OAuth.OAUTH_TOKEN_SECRET;
import static net.oauth.OAuth.OAUTH_VERIFIER;
import static net.oauth.OAuth.Problems.PARAMETER_ABSENT;
import static net.oauth.OAuth.Problems.PERMISSION_DENIED;
import static net.oauth.OAuth.Problems.PERMISSION_UNKNOWN;
import static net.oauth.OAuth.Problems.TOKEN_REJECTED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class OAuthDanceTest extends OAuthTestBase {
    @After
    public void teardown() {
        logout(PRODUCT);
    }

    @Test
    public void assertThatConsumerCanGetRequestToken() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        OAuthMessage message = client.getRequestTokenResponse(accessor, "POST",
                ImmutableList.of(new Parameter(OAUTH_CALLBACK, CALLBACK_URI)));

        assertNotNull("request token", accessor.requestToken);
        assertThat(message.getParameter(OAUTH_CALLBACK_CONFIRMED), is(equalTo("true")));
    }

    @Test
    public void assertThatNonAuthenticatedUserGetsRedirectedToServiceProviderLogin() throws Exception {
        final String authorizeUrl = AUTHORIZE_URL + "?oauth_token=" + NON_AUTHORIZED_REQUEST_TOKEN_FOR_AUTHORIZING;

        final HttpGet get = new HttpGet(authorizeUrl);
        final HttpClient client = HttpClientBuilder.create().disableRedirectHandling().build();

        final HttpResponse response = client.execute(get);

        assertThat(response.getStatusLine().getStatusCode(), is(HttpStatus.SC_MOVED_TEMPORARILY));
        assertThat(response.getFirstHeader("Location"), notNullValue());
        assertThat(response.getFirstHeader("Location").getValue(), is(startsWith(LOGIN_URL + "?os_destination=" + uriEncode(authorizeUrl))));
    }

    @Test
    public void assertThatUserCanApproveAuthorizationAndWillBeRedirectedToConsumer() throws Exception {
        login(User.FRED.username, User.FRED.password, PRODUCT);
        AuthorizePage authorizePage = PRODUCT.visit(AuthorizePage.class, NON_AUTHORIZED_REQUEST_TOKEN_FOR_AUTHORIZING);

        assertThat(authorizePage.getConsumerName(), is(equalTo("Hardcoded Consumer")));
        assertThat(authorizePage.isApprovePresentAndEnabled(), is(true));

        authorizePage.clickApprove();
        // NB. Since replacement of HtmlUnit with Webdriver as the method to test, we can no longer confirm the response status code.
        // or at least not without considerable work http://code.google.com/p/selenium/issues/detail?id=141
        assertThat(
                authorizePage.getCurrentUrl(),
                allOf(
                        startsWith("http://consumer/callback?"),
                        containsString("oauth_token=" + NON_AUTHORIZED_REQUEST_TOKEN_FOR_AUTHORIZING),
                        containsString("oauth_verifier=") // the verifier value is randomly generated, no way to guess it
                )
        );
    }

    @Test
    public void assertThatUserCanApproveAuthorizationWithoutCallbacksAndVerifierWillBeDisplayed() throws Exception {
        login(User.FRED);
        AuthorizePage authorizePage = PRODUCT.visit(AuthorizePage.class, NON_AUTHORIZED_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_AUTHORIZING);

        assertThat(authorizePage.getConsumerName(), is(equalTo("Hardcoded Consumer - Without Callback")));
        assertThat(authorizePage.isApprovePresentAndEnabled(), is(true));

        authorizePage.clickApprove();
        // NB. Since replacement of HtmlUnit with Webdriver as the method to test, we can no longer confirm the response status code.
        // or at least not without considerable work http://code.google.com/p/selenium/issues/detail?id=141
        assertThat(authorizePage.getPageSource(), allOf(
                containsString("You have successfully "),
                containsString("'Hardcoded Consumer - Without Callback'"),
                containsString("Your verification code is '")
        ));
    }

    @Test
    public void assertThatUserCanDenyAuthorizationAndWillBeRedirectedToConsumer() throws Exception {
        login(User.FRED.username, User.FRED.password, PRODUCT);
        AuthorizePage authorizePage = PRODUCT.visit(AuthorizePage.class, NON_AUTHORIZED_REQUEST_TOKEN_FOR_DENYING);

        assertThat(authorizePage.getConsumerName(), is(equalTo("Hardcoded Consumer")));
        assertThat(authorizePage.isDenyPresentAndEnabled(), is(true));

        authorizePage.clickDeny();
        // NB. Since replacement of HtmlUnit with Webdriver as the method to test, we can no longer confirm the response status code.
        // or at least not without considerable work http://code.google.com/p/selenium/issues/detail?id=141
        assertThat(
                authorizePage.getCurrentUrl(),
                allOf(
                        startsWith("http://consumer/callback?"),
                        containsString("oauth_token=" + NON_AUTHORIZED_REQUEST_TOKEN_FOR_DENYING),
                        containsString("oauth_verifier=") // the verifier value is randomly generated, no way to guess it
                )
        );
    }

    @Test
    public void assertThatUserCanDenyAuthorizationWithoutCallbacksAndDeniedMessageIsDisplayed() throws Exception {
        login(User.FRED.username, User.FRED.password, PRODUCT);
        AuthorizePage authorizePage = PRODUCT.visit(AuthorizePage.class, NON_AUTHORIZED_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_DENYING);

        assertThat(authorizePage.getConsumerName(), is(equalTo("Hardcoded Consumer - Without Callback")));
        assertThat(authorizePage.isDenyPresentAndEnabled(), is(true));

        authorizePage.clickDeny();
        assertThat(authorizePage.getPageSource(),
                containsString("You have denied access to 'Hardcoded Consumer - Without Callback'")
        );
    }

    @Test
    public void assertThatConsumerCannotSwapAuthorizedRequestTokenForAccessTokenIfRequestConsumerKeyDoesNotMatchToken() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        accessor.requestToken = AUTHORIZED_REQUEST_TOKEN_FOR_SWAPPING;
        accessor.tokenSecret = AUTHORIZED_REQUEST_TOKEN_SECRET_FOR_SWAPPING;

        try {
            // the consumer key specified here is different from the key stored in the token so we simulate the token
            // being used by a different consumer.
            client.getAccessToken(accessor, "POST", ImmutableList.of(new Parameter("oauth_consumer_key", "hardcoded-2lo-consumer"),
                    new Parameter(OAuth.OAUTH_VERIFIER, AUTHORIZED_REQUEST_TOKEN_VERIFIER_FOR_SWAPPING)));
            fail("exception expected");
        } catch (OAuthProblemException ope) {
            assertThat(ope.getProblem(), is(equalTo(TOKEN_REJECTED)));
        }
    }

    @Test
    public void assertThatConsumerCanSwapAuthorizedRequestTokenForAccessToken() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        accessor.requestToken = AUTHORIZED_REQUEST_TOKEN_FOR_SWAPPING;
        accessor.tokenSecret = AUTHORIZED_REQUEST_TOKEN_SECRET_FOR_SWAPPING;
        OAuthMessage message = client.getAccessToken(accessor, "POST",
                ImmutableList.of(new Parameter(OAuth.OAUTH_VERIFIER, AUTHORIZED_REQUEST_TOKEN_VERIFIER_FOR_SWAPPING)));

        assertNotNull(message.getToken());
        assertNotNull(message.getParameter(OAUTH_TOKEN_SECRET));
        assertNotNull(message.getParameter(OAUTH_SESSION_HANDLE));
        assertNotNull(message.getParameter(OAUTH_EXPIRES_IN));
        assertNotNull(message.getParameter(OAUTH_AUTHORIZATION_EXPIRES_IN));
    }

    @Test(expected = OAuthProblemException.class)
    public void assertThatConsumerCanNotSwapUnauthorizedRequestTokenForAccessToken() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        accessor.requestToken = SPARE_NON_AUTHORIZED_REQUEST_TOKEN;
        accessor.tokenSecret = SPARE_NON_AUTHORIZED_REQUEST_TOKEN_SECRET;
        try {
            client.getAccessToken(accessor, "POST",
                    ImmutableList.of(new Parameter(OAuth.OAUTH_VERIFIER, SPARE_AUTHORIZED_REQUEST_TOKEN_VERIFIER)));
        } catch (OAuthProblemException e) {
            assertThat(e.getProblem(), is(equalTo(PERMISSION_UNKNOWN)));
            throw e;
        }
    }

    @Test
    public void assertThatConsumerCanAccessRestrictedResourcesWithAccessToken() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        accessor.accessToken = ACCESS_TOKEN;
        accessor.tokenSecret = ACCESS_TOKEN_SECRET;
        OAuthMessage response = client.invoke(accessor, WHOAMI_URL, Collections.<Map.Entry<?, ?>>emptySet());
        assertThat(response.readBodyAsString(), is(equalTo("fred")));
    }

    @Test
    public void assertThatConsumerCannotAccessRestrictedResourcesWithAccessTokenIf3LOForThatConsumerIsOff() throws Exception {
        OAuthAccessor accessor = createAccessor("hardcoded-2lo-only-consumer");
        OAuthClient client = createClient();

        accessor.accessToken = ACCESS_TOKEN_ON_NON_3LO_CONSUMER;
        accessor.tokenSecret = ACCESS_TOKEN_SECRET_ON_NON_3LO_CONSUMER;

        try {
            client.invoke(accessor, WHOAMI_URL, Collections.<Map.Entry<?, ?>>emptySet());
        } catch (OAuthProblemException ope) {
            assertThat(ope.getProblem(), is(equalTo(PERMISSION_DENIED)));
        }
    }

    @Test
    public void assertThatConsumerCannotAccessRestrictedResourcesWithAccessTokenIfRequestConsumerKeyDoesNotMatchToken() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        accessor.accessToken = ACCESS_TOKEN;
        accessor.tokenSecret = ACCESS_TOKEN_SECRET;
        try {
            // the consumer key specified here is different from the key stored in the token so we simulate the token
            // being used by a different consumer.
            client.invoke(accessor, WHOAMI_URL, ImmutableList.of(new Parameter("oauth_consumer_key", "hardcoded-2lo-consumer")));
            fail("exception expected");
        } catch (OAuthProblemException ope) {
            assertThat(ope.getProblem(), is(equalTo(TOKEN_REJECTED)));
        }
    }

    @Test
    public void assertThatSuccessfulRequestIsAnnotatedAsOAuthRequest() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        accessor.accessToken = ACCESS_TOKEN;
        accessor.tokenSecret = ACCESS_TOKEN_SECRET;
        OAuthMessage response = client.invoke(accessor, STRICT_SECURITY_URL, Collections.<Map.Entry<?, ?>>emptySet());
        assertThat(response.getHeader("X-OAUTH-REQUEST-ANNOTATED"), is(equalTo("true")));
    }

    @Test
    public void assertThatDanceCanBePerformedFromStartToFinish() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        OAuthMessage requestTokenResponse = client.getRequestTokenResponse(accessor, "POST",
                ImmutableList.of(new Parameter(OAUTH_CALLBACK, CALLBACK_URI)));
        assertNotNull("request token", accessor.requestToken);
        assertThat(requestTokenResponse.getParameter(OAUTH_CALLBACK_CONFIRMED), is(equalTo("true")));

        login(User.FRED.username, User.FRED.password, PRODUCT);
        AuthorizePage authorizePage = PRODUCT.visit(AuthorizePage.class, accessor.requestToken);

        assertThat(authorizePage.getConsumerName(), is(equalTo("Hardcoded Consumer")));
        assertThat(authorizePage.isApprovePresentAndEnabled(), is(true));

        authorizePage.clickApprove();

        String callback = authorizePage.getCurrentUrl();
        assertThat(
                callback,
                allOf(
                        startsWith("http://consumer/callback?"),
                        containsString("oauth_token=" + accessor.requestToken),
                        containsString("oauth_verifier=") // the verifier value is randomly generated, no way to guess it
                )
        );

        String verifier = callback.substring(callback.indexOf(OAUTH_VERIFIER) + OAUTH_VERIFIER.length() + 1).split("\\&")[0];
        client.getAccessToken(accessor, "POST",
                ImmutableList.of(new Parameter(OAUTH_VERIFIER, verifier)));
        assertNotNull("access token", accessor.accessToken);

        OAuthMessage message = client.invoke(accessor, WHOAMI_URL, Collections.<Map.Entry<?, ?>>emptySet());
        assertThat(message.readBodyAsString(), is(equalTo("fred")));
    }

    @Test
    public void assertThatNonAuthenticatedUserCanAccessOpenResourceAndNoWWWAuthenticateHeaderIsSent() throws Exception {
        final HttpGet get = new HttpGet(WHOAMI_URL);
        final HttpClient client = HttpClientBuilder.create().build();

        final HttpResponse response = client.execute(get);

        assertThat(response.getStatusLine().getStatusCode(), is(HttpStatus.SC_OK));
        assertThat(response.getFirstHeader("WWW-Authenticate"), nullValue());
    }

    @Test
    public void assertThatNonAuthenticatedUserCanNotAccessStrictSecurityResourcesAndWWWAuthenticateHeaderIsSent() throws Exception {
        final HttpGet get = new HttpGet(STRICT_SECURITY_URL);
        final HttpClient client = HttpClientBuilder.create().build();

        final HttpResponse response = client.execute(get);

        assertThat(response.getStatusLine().getStatusCode(), is(HttpStatus.SC_UNAUTHORIZED));
        assertThat(response.getFirstHeader("WWW-Authenticate"), notNullValue());
        assertThat(response.getFirstHeader("WWW-Authenticate").getValue(), containsString("OAuth"));
    }

    @Test
    public void assertThatTryingToAuthorizeAnAccessTokenResultsInAnErrorWhenGettingAuthorizationForm() throws Exception {
        final HttpGet get = new HttpGet(AUTHORIZE_URL + "?oauth_token=" + ACCESS_TOKEN);
        final HttpClient client = HttpClientBuilder.create().build();

        final HttpResponse response = client.execute(get);

        assertThat(response.getStatusLine().getStatusCode(), is(HttpStatus.SC_UNAUTHORIZED));
        assertThat(response.getFirstHeader("WWW-Authenticate"), notNullValue());
        assertThat(response.getFirstHeader("WWW-Authenticate").getValue(), containsString("OAuth"));
        assertThat(EntityUtils.toString(response.getEntity()), allOf(
                containsString("The request token you are trying to"),
                containsString("is not valid")));
    }

    @Test
    public void assertThatTryingToAuthorizeAnAccessTokenResultsInAnErrorWhenPostingAuthorizationForm() throws Exception {
        final HttpPost post = new HttpPost(AUTHORIZE_URL);
        post.setEntity(new UrlEncodedFormEntity(ImmutableList.of(new BasicNameValuePair("oauth_token", AUTHORIZE_URL))));

        final HttpClient client = HttpClientBuilder.create().build();

        final HttpResponse response = client.execute(post);

        assertThat(response.getStatusLine().getStatusCode(), is(HttpStatus.SC_UNAUTHORIZED));
        assertThat(response.getFirstHeader("WWW-Authenticate"), notNullValue());
        assertThat(response.getFirstHeader("WWW-Authenticate").getValue(), containsString("OAuth"));
        assertThat(EntityUtils.toString(response.getEntity()), allOf(
                containsString("The request token you are trying to"),
                containsString("is not valid")));
    }

    @Test
    public void assertThatTryingToAuthorizeAnAlreadyAuthorizedRequestTokenResultsInAnErrorWhenGettingAuthorizationForm() throws Exception {
        final HttpGet get = new HttpGet(AUTHORIZE_URL + "?oauth_token=" + SPARE_AUTHORIZED_REQUEST_TOKEN);
        final HttpClient client = HttpClientBuilder.create().build();

        final HttpResponse response = client.execute(get);

        assertThat(response.getStatusLine().getStatusCode(), is(HttpStatus.SC_UNAUTHORIZED));
        assertThat(response.getFirstHeader("WWW-Authenticate"), notNullValue());
        assertThat(response.getFirstHeader("WWW-Authenticate").getValue(), containsString("OAuth"));
        assertThat(EntityUtils.toString(response.getEntity()), containsString("You have already approved or denied this access request"));
    }

    @Test
    public void assertThatTryingToAuthorizeAnAlreadyAuthorizedRequestTokenResultsInAnErrorWhenPostingAuthorizationForm() throws Exception {
        final HttpPost post = new HttpPost(AUTHORIZE_URL);
        post.setEntity(new UrlEncodedFormEntity(ImmutableList.of(new BasicNameValuePair("oauth_token", SPARE_AUTHORIZED_REQUEST_TOKEN))));

        final HttpClient client = HttpClientBuilder.create().build();

        final HttpResponse response = client.execute(post);

        assertThat(response.getStatusLine().getStatusCode(), is(HttpStatus.SC_UNAUTHORIZED));
        assertThat(response.getFirstHeader("WWW-Authenticate"), notNullValue());
        assertThat(response.getFirstHeader("WWW-Authenticate").getValue(), containsString("OAuth"));
        assertThat(EntityUtils.toString(response.getEntity()), containsString("You have already approved or denied this access request"));
    }

    @Test(expected = OAuthProblemException.class)
    public void assertThatTryingToSwapAccessTokenWithoutVerifierCausesParameterAbsentErrorResponse() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        accessor.requestToken = SPARE_AUTHORIZED_REQUEST_TOKEN;
        accessor.tokenSecret = SPARE_AUTHORIZED_REQUEST_TOKEN_SECRET;
        try {
            client.getAccessToken(accessor, "POST", null);
        } catch (OAuthProblemException e) {
            assertThat(e.getProblem(), is(equalTo(PARAMETER_ABSENT)));
            throw e;
        }
    }

    @Test
    public void assertThatConsumerCanRenewAccessTokensWithinValidAuthorizationSession() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        accessor.requestToken = RENEWABLE_ACCESS_TOKEN;
        OAuthMessage message = client.getAccessToken(accessor, "POST",
                ImmutableList.of(new Parameter("oauth_session_handle", RENEWABLE_ACCESS_TOKEN_SESSION_HANDLE)));

        assertNotNull(message.getToken());
        assertNotNull(message.getParameter(OAUTH_TOKEN_SECRET));
        assertNotNull(message.getParameter(OAUTH_SESSION_HANDLE));
        assertNotNull(message.getParameter(OAUTH_EXPIRES_IN));
        assertNotNull(message.getParameter(OAUTH_AUTHORIZATION_EXPIRES_IN));
    }

    @Test(expected = OAuthProblemException.class)
    public void assertThatConsumerCannotRenewAccessTokensWhenAuthorizationSessionIsInvalid() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        accessor.requestToken = NON_RENEWABLE_ACCESS_TOKEN;
        try {
            client.getAccessToken(accessor, "POST",
                    ImmutableList.of(new Parameter("oauth_session_handle", NON_RENEWABLE_ACCESS_TOKEN_SESSION_HANDLE)));
        } catch (OAuthProblemException e) {
            assertThat(e.getProblem(), is(equalTo(PERMISSION_DENIED)));
            throw e;
        }
    }

    // OAuth v1.0 backward compat tests
    @Test
    public void assertThatConsumerCanGetVersion1RequestTokenWithoutCallback() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        OAuthMessage message = client.getRequestTokenResponse(accessor, "POST", null);
        assertNotNull("request token", accessor.requestToken);
        assertThat(message.getParameter(OAUTH_CALLBACK_CONFIRMED), is(nullValue()));
    }

    @Test
    public void assertThatUserCanApproveAuthorizationAndWillBeRedirectedToConsumerSpecifiedByRequestParameterIfTokenIsAVersion1Token() throws Exception {
        login(User.FRED);
        AuthorizePage authorizePage = PRODUCT.visit(AuthorizePage.class, NON_AUTHORIZED_V1_REQUEST_TOKEN_FOR_AUTHORIZING +
                "&" + OAUTH_CALLBACK + "=http://request/callback");

        assertThat(authorizePage.getConsumerName(), is(equalTo("Hardcoded Consumer")));
        assertThat(authorizePage.isApprovePresentAndEnabled(), is(true));

        authorizePage.clickApprove();
        // NB. Since replacement of HtmlUnit with Webdriver as the method to test, we can no longer confirm the response status code.
        // or at least not without considerable work http://code.google.com/p/selenium/issues/detail?id=141
        assertThat(
                authorizePage.getCurrentUrl(),
                allOf(
                        startsWith("http://request/callback?"),
                        containsString("oauth_token=" + NON_AUTHORIZED_V1_REQUEST_TOKEN_FOR_AUTHORIZING),
                        not(containsString("oauth_verifier="))
                )
        );
    }

    @Test
    public void assertThatUserCanApproveAuthorizationForVersion1TokenWithoutCallbacksAndMessageWillBeDisplayed() throws Exception {
        login(User.FRED);
        AuthorizePage authorizePage = PRODUCT.visit(AuthorizePage.class, NON_AUTHORIZED_V1_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_AUTHORIZING);

        assertThat(authorizePage.getConsumerName(), is(equalTo("Hardcoded Consumer - Without Callback")));
        assertThat(authorizePage.isApprovePresentAndEnabled(), is(true));

        authorizePage.clickApprove();
        // NB. Since replacement of HtmlUnit with Webdriver as the method to test, we can no longer confirm the response status code.
        // or at least not without considerable work http://code.google.com/p/selenium/issues/detail?id=141
        assertThat(authorizePage.getPageSource(), allOf(
                containsString("You have successfully"),
                containsString("'Hardcoded Consumer - Without Callback'"),
                containsString("Please close this browser window and click continue in the client.")
        ));
    }

    @Test
    public void assertThatUserCanDenyAuthorizationOfVersion1TokenAndWillBeRedirectedToRequestedCallback() throws Exception {
        login(User.FRED);
        AuthorizePage authorizePage = PRODUCT.visit(AuthorizePage.class, NON_AUTHORIZED_V1_REQUEST_TOKEN_FOR_DENYING +
                "&" + OAUTH_CALLBACK + "=http://request/callback");

        assertThat(authorizePage.getConsumerName(), is(equalTo("Hardcoded Consumer")));
        assertThat(authorizePage.isDenyPresentAndEnabled(), is(true));

        authorizePage.clickDeny();
        // NB. Since replacement of HtmlUnit with Webdriver as the method to test, we can no longer confirm the response status code.
        // or at least not without considerable work http://code.google.com/p/selenium/issues/detail?id=141
        assertThat(
                authorizePage.getCurrentUrl(),
                allOf(
                        startsWith("http://request/callback?"),
                        containsString("oauth_token=" + NON_AUTHORIZED_V1_REQUEST_TOKEN_FOR_DENYING),
                        not(containsString("oauth_verifier=")) // the verifier value is randomly generated, no way to guess it
                )
        );
    }

    @Test
    public void assertThatUserCanDenyAuthorizationOfVersion1TokenWithoutCallbacksAndDeniedMessageIsDisplayed() throws Exception {
        login(User.FRED);
        AuthorizePage authorizePage = PRODUCT.visit(AuthorizePage.class, NON_AUTHORIZED_V1_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_DENYING);
        assertThat(authorizePage.getConsumerName(), is(equalTo("Hardcoded Consumer - Without Callback")));
        assertThat(authorizePage.isDenyPresentAndEnabled(), is(true));

        authorizePage.clickDeny();
        // NB. Since replacement of HtmlUnit with Webdriver as the method to test, we can no longer confirm the response status code.
        // or at least not without considerable work http://code.google.com/p/selenium/issues/detail?id=141

        assertThat(authorizePage.getPageSource(),
                containsString("You have denied access to 'Hardcoded Consumer - Without Callback'")
        );
    }

    @Test
    public void assertThatConsumerCanSwapAuthorizedVersion1RequestTokenForAccessTokenWithoutVerifier() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        accessor.requestToken = AUTHORIZED_V1_REQUEST_TOKEN_FOR_SWAPPING;
        accessor.tokenSecret = AUTHORIZED_REQUEST_TOKEN_SECRET_FOR_SWAPPING;
        OAuthMessage message = client.getAccessToken(accessor, "POST", null);

        assertNotNull(message.getToken());
        assertNotNull(message.getParameter(OAUTH_TOKEN_SECRET));
    }

    @Test
    public void assertThatVersion1DanceCanBePerformedFromStartToFinish() throws Exception {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        OAuthMessage requestTokenResponse = client.getRequestTokenResponse(accessor, "POST", null);
        assertNotNull("request token", accessor.requestToken);
        assertThat(requestTokenResponse.getParameter(OAUTH_CALLBACK_CONFIRMED), is(nullValue()));

        login(User.FRED);
        AuthorizePage authorizePage = PRODUCT.visit(AuthorizePage.class, accessor.requestToken +
                "&" + OAUTH_CALLBACK + "=http://request/callback");

        assertThat(authorizePage.getConsumerName(), is(equalTo("Hardcoded Consumer")));
        assertThat(authorizePage.isApprovePresentAndEnabled(), is(true));

        authorizePage.clickApprove();
        // NB. Since replacement of HtmlUnit with Webdriver as the method to test, we can no longer confirm the response status code.
        // or at least not without considerable work http://code.google.com/p/selenium/issues/detail?id=141
        String callback = authorizePage.getCurrentUrl();
        assertThat(
                callback,
                allOf(
                        startsWith("http://request/callback?"),
                        containsString("oauth_token=" + accessor.requestToken),
                        not(containsString("oauth_verifier="))
                )
        );

        client.getAccessToken(accessor, "POST", null);
        assertNotNull("access token", accessor.accessToken);

        OAuthMessage message = client.invoke(accessor, WHOAMI_URL, Collections.<Map.Entry<?, ?>>emptySet());
        assertThat(message.readBodyAsString(), is(equalTo("fred")));
    }

    protected final String uriEncode(String uriComponent) throws UnsupportedEncodingException {
        return URLEncoder.encode(uriComponent, "UTF-8");
    }
}
